//
//  Shortcut.swift
//  Brincando
//
//  Created by Deway on 20/01/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

final class Shortcut: Object, Mappable {
    
    dynamic var id = 0
    var module: Module?
    dynamic var title = ""
    dynamic var icon = ""
    dynamic var position = 0
    dynamic var active = false
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
    }
    
}


//
//  EventoDescricaoViewController.swift
//  Brincando
//
//  Created by Joaquim Prince on 23/01/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit

class EventoDescricaoViewController: UIViewController {

    @IBAction func rootTouched(_ sender: AnyObject) {
        
        let _ = navigationController?.popToRootViewController(animated: true)
        
    }
    
    @IBAction func completedTouched(_ sender: AnyObject) {
        
        //let eventoStoryboard = UIStoryboard(name: "Evento", bundle: nil)
        
        //let eventoVC = eventoStoryboard.instantiateViewController(withIdentifier: "EventoViewController")
        
        //show(eventoVC, sender: self)
        
        //let _ = navigationController?.popViewController(animated: true)
        
        let eventoStoryboard = UIStoryboard(name: "Evento", bundle: nil)
        
        let eventoVC = eventoStoryboard.instantiateViewController(withIdentifier: "EventoViewController")
        
        let _ = navigationController?.popToViewController(eventoVC, animated: true)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}

//
//  HomeViewController.swift
//  Brincando
//
//  Created by Deway on 17/01/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit

class HomeViewController: ViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var myTableView: UITableView!
    
    func editarTouched(sender: IndexPathButton) {
        
        performSegue(withIdentifier: "evento", sender: self)
    }
    
    func excluirTouched(sender: IndexPathButton) {
        print(sender.indexPath.row)
    }
    
    var array: [String] = ["Evento ", "Palestra", "Nao sei dizer o nome disso"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Home"
        
        myTableView.estimatedRowHeight = 55
        
        myTableView.register(UINib(nibName: "DefaultTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        myTableView.register(UINib(nibName: "HeaderView", bundle: nil), forCellReuseIdentifier: "header")
        
        myTableView.register(UINib(nibName: "FooterView", bundle: nil), forCellReuseIdentifier: "footer")
        
        myTableView.tableFooterView = UIView(frame: CGRect.zero)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = myTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DefaultTableViewCell
        
        cell.editarButton.indexPath = indexPath
        
        cell.excluirButton.indexPath = indexPath
        
        cell.editarButton.addTarget(self, action: #selector(editarTouched(sender:)), for: .touchUpInside)
        
        cell.excluirButton.addTarget(self, action: #selector(excluirTouched(sender:)), for: .touchUpInside)

        cell.titleLabel.text = array[(indexPath as NSIndexPath).row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = myTableView.cellForRow(at: indexPath) as! DefaultTableViewCell
        
        if cell.estimatedRowHeight == 55 {
            
            cell.heightView.constant = 45
            
            myTableView.estimatedRowHeight = 128
            
            cell.estimatedRowHeight = 128
            
        } else {
            
            cell.heightView.constant = 0
            
            myTableView.estimatedRowHeight = 55
            
            cell.estimatedRowHeight = 55
            
        }
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            
            if cell.arrowImageView.isRotated {
                
                let targetView = cell.arrowImageView
                
                UIView.animate(withDuration: 0.05, animations: { () -> Void in
                    
                    targetView?.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
                    
                    }, completion: { (complete: Bool) -> Void in
                        
                        UIView.animate(withDuration: 0.05, animations: { () -> Void in
                            
                            targetView?.transform = (targetView?.transform.rotated(by: CGFloat(M_PI_2)))!
    
                            }, completion: { (complete: Bool) -> Void in
                                
                                UIView.animate(withDuration: 0.05, animations: { () -> Void in
                                    
                                    targetView?.transform = CGAffineTransform(rotationAngle: 0)
                                    
                                    }, completion: { (complete: Bool) -> Void in
                                        
                                })
                                
                        })
                        
                })
                
                cell.arrowImageView.isRotated = false
                
            } else {
                
                let targetView = cell.arrowImageView
                
                UIView.animate(withDuration: 0.05, animations: { () -> Void in
                    
                    targetView?.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_4))
                    
                    }, completion: { (complete: Bool) -> Void in
                        
                        UIView.animate(withDuration: 0.05, animations: { () -> Void in
                            
                            targetView?.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_2))
                            
                            }, completion: { (complete: Bool) -> Void in
                                
                                UIView.animate(withDuration: 0.05, animations: { () -> Void in
                                    
                                    targetView?.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI))
                                    
                                    }, completion: { (complete: Bool) -> Void in
                                        
                                })
                                
                        })
                        
                })
                
                cell.arrowImageView.isRotated = true
                
                
                
            }
        
        })
        
        myTableView.deselectRow(at: indexPath, animated: true)
        
        myTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = Bundle.main.loadNibNamed("HeaderView", owner: nil, options: nil)?.first as! HeaderView
        
        headerView.titleLabel.text = "Evento"
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        let headerView = Bundle.main.loadNibNamed("HeaderView", owner: nil, options: nil)?.first as! HeaderView
        
        return headerView.bounds.height
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let backBarButton = UIBarButtonItem()
        
        backBarButton.title = ""
        
        navigationItem.backBarButtonItem = backBarButton
        
    }

}

//
//  EventoViewController.swift
//  Brincando
//
//  Created by Joaquim Prince on 20/01/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit

class EventoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var eventoTableView: UITableView!
    
    //MARK:
    
    var eventos: [String] = ["a", "b", "c", "d"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        eventoTableView.register(UINib(nibName: "LabelTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        eventoTableView.tableFooterView = UIView(frame: CGRect.zero)

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return eventos.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = eventoTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LabelTableViewCell
        
        cell.titleLabel.text = eventos[(indexPath as NSIndexPath).row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "descricao", sender: self)
        
    }

}

//
//  Module.swift
//  Brincando
//
//  Created by Deway on 20/01/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper

class Module: Object, Mappable {
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
    }
}

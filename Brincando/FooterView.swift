//
//  FooterView.swift
//  Brincando
//
//  Created by Deway on 17/01/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit

class FooterView: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

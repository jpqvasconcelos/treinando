//
//  DefaultTableViewCell.swift
//  Brincando
//
//  Created by Deway on 17/01/17.
//  Copyright © 2017 Deway. All rights reserved.
//

import UIKit

class DefaultTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var heightView: NSLayoutConstraint!
    
    @IBOutlet weak var arrowImageView: RotateImage!
    
    @IBOutlet weak var editarButton: IndexPathButton!
    
    @IBOutlet weak var excluirButton: IndexPathButton!
    
    var estimatedRowHeight: CGFloat = 55
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
